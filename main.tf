resource "google_pubsub_topic" "logging_topic" {
  name = var.topic_name

  project = var.project_id

  message_storage_policy {
    allowed_persistence_regions = var.allowed_persistence_regions
  }
}

resource "google_monitoring_notification_channel" "logging_channel" {
  display_name = var.channel_display_name

  type = "pubsub"

  labels = {
    topic = google_pubsub_topic.logging_topic.id
  }
}

resource "google_storage_bucket" "source_code" {
  project = var.project_id

  name     = var.bucket_name
  location = var.region
  uniform_bucket_level_access = true
}

data "archive_file" "trigger_function_zip_test" {
  type        = "zip"
  source_dir = var.archive_source_dir
  output_path = var.archive_output_path
}

resource "google_storage_bucket_object" "trigger_function_zip" {
  name   = var.archive_object_name
  bucket = google_storage_bucket.source_code.name
  source = var.object_source_path
}

