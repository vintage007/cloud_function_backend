output "topic_id" {
  value = google_pubsub_topic.logging_topic.id
}

output "topic_name" {
  value = google_pubsub_topic.logging_topic.name
}

output "channel_id" {
  value = google_monitoring_notification_channel.logging_channel.id
}

output "bucket_name" {
  value = google_storage_bucket.source_code.name
}

output "bucket_object_name" {
  value = google_storage_bucket_object.trigger_function_zip.name
}
