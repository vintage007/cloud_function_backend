variable "project_id" { default = "" }

variable "region" { default = "" }

variable "topic_name" { default = "" }

variable "allowed_persistence_regions" { default = "" }

variable "channel_display_name" { default = "" }

variable "bucket_name" { default = "" }

variable "archive_source_dir" { default = "" }

variable "archive_output_path" { default = "" }

variable "archive_object_name" { default = "" }

variable "object_source_path" { default = "" }


